<%-- 
    Document   : agenda
    Created on : 07/11/2018, 21:20:43
    Author     : emerson
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <link href="resources/css/panel.css" rel="stylesheet" type="text/css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Agenda</title>
    </head>
    <body>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
        
        <div class="container">
    <div class="row">
    
    <p></p>
    <h1 style="text-align: center;">Agenda</h1>
    
    <p> </p><p> </p>
    
        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default panel-table">
              <div class="panel-heading">
                <div class="row">
                  <div class="col col-xs-6">
                    <h3 class="panel-title">Lista de Compromissos</h3>
                  </div>
                  <div class="col col-xs-6 text-right">
                    <a class="btn btn-sm btn-primary btn-create" href="novoCompromisso">Novo Compromisso</a>
                    <a class="btn btn-sm btn-danger btn-create" href="logout">Sair</a>
                  </div>
                </div>
              </div>  
              <div class="panel-body">
                <table class="table table-striped table-bordered table-list">
                  <thead>
                    <tr>
                        <th><em class="fa fa-cog"></em></th>
                        <th class="hidden-xs">Codigo</th>
                        <th>Data Compromisso</th>
                        <th>Assunto</th>
                        <th>Descrição</th>
                        <th>Data Cadastro</th>
                    </tr> 
                  </thead>
                  <tbody>
                      <c:forEach var="c" items="${listaCompromisso}">
                          <tr>
                            <td align="center">
                              <a class="btn btn-default" href="editarCompromisso?codigo=${c.codigo}"><em class="fa fa-pencil"></em></a>
                              <a class="btn btn-danger" href="removerCompromisso?codigo=${c.codigo}"><em class="fa fa-trash"></em></a>
                            </td>
                            <td class="hidden-xs">${c.codigo}</td>
                            <td>${c.dtCompromisso}</td>
                            <td>${c.assunto}</td>
                            <td>${c.descricao}</td>
                            <td>${c.dtCadastro}</td>
                          </tr>
                      </c:forEach>
                        </tbody>
                </table>
            
              </div>
              <div class="panel-footer">
                <!--<div class="row">
                  <div class="col col-xs-4">Page 1 of 5
                  </div>
                  <div class="col col-xs-8">
                    <ul class="pagination hidden-xs pull-right">
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">4</a></li>
                      <li><a href="#">5</a></li>
                    </ul>
                    <ul class="pagination visible-xs pull-right">
                        <li><a href="#">«</a></li>
                        <li><a href="#">»</a></li>
                    </ul>
                  </div>
                </div>!-->
              </div>
            </div>

</div></div></div>
    </body>
</html>
