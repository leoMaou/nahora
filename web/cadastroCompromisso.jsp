<%-- 
    Document   : cadastroCompromisso
    Created on : 12/11/2018, 23:40:23
    Author     : emerson
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.Date" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <link href="resources/css/estilo.css" rel="stylesheet" type="text/css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cadastro de Compromisso</title>
    </head>
    <body>
        <div class="container">
        <h1>Cadastro de Compromisso</h1>
        <div class="col-md-12">
        ${mensagem}
        <form method="POST" action="salvarCompromisso">
            <div class="form-group">
                <input name="codigo" id="codigo" type="text" hidden value="${compromissoedicao.codigo}">
                <label>Assunto</label>
                <input name="assunto" type="text" class="form-control" placeholder="Digite o assunto aqui." value="${compromissoedicao.assunto}">
            </div>
            <div class="form-group">
                <label>Descrição</label>
                <textarea name="descricao" class="form-control" rows="3">${compromissoedicao.descricao}</textarea>
            </div>
            <div class="form-group">
                <label >Data Compromisso</label>
                <div class="col-md-4">
                <input class="form-control" type="date" value="${compromissoedicao.dtCompromisso}" name="dtCompromisso" format="dd/MM/yyyy"> <br>
                </div>
            </div>
            <div class="form-group">
                <input name="situacao" class="form-check-input" type="checkbox" value="${compromissoedicao.concluido}" >Concluir Compromisso<br>
            </div>
            <button class="btn btn-primary" >Salvar</button>
        </form>
         </div>
        </div>
    </body>
</html>
