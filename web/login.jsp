<%-- 
    Document   : login
    Created on : 07/11/2018, 20:48:51
    Author     : emerson
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <link href="resources/css/nuvem.css" rel="stylesheet" type="text/css"/>
        <title>Login</title>
    </head>
    <body>
        <div class="container">
        <div id="login-row" class="row justify-content-center align-items-center">
            <div id="login-column" class="col-md-6">
                	${mensagem}
                <div class="box">
                    <div class="shape1"></div>
                    <div class="shape2"></div>
                    <div class="shape3"></div>
                    <div class="shape4"></div>
                    <div class="shape5"></div>
                    <div class="shape6"></div>
                    <div class="shape7"></div>
                    <div class="float">
                        <form class="form" action="fazerLogin" method="post">
                            <div class="form-group">
                                <label for="username" class="text-white">Email:</label><br>
                                <input type="text" name="email" id="username" class="form-control" value="${usuarioLogin.email}">
                            </div>
                            <div class="form-group">
                                <label for="password" class="text-white">Senha:</label><br>
                                <input type="password" name="senha" id="password" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="submit" name="fazerLogin" class="btn btn-info btn-md" value="Login">
                            </div>
                        </form>
                    </div>
                </div>
                <a class="btn btn-link btn-md" href="cadastrarUsuario.jsp">Novo Usuario</a>
            </div>
        </div>
    </body>
</html>
