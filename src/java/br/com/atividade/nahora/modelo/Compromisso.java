/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.atividade.nahora.modelo;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author emerson
 */
public class Compromisso {

    private int codigo;
    private int codigoUsuario;
    private String assunto;
    private String descricao;
    private Date dtCompromisso;
    private Date dtCadastro;
    private String situacao;
    
    public Date getToDateAfterDays(Integer day) {
        Date nowdate = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(nowdate);
        cal.add(Calendar.DATE, day);
        return cal.getTime();
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(int codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public String getAssunto() {
        return assunto;
    }

    public void setAssunto(String assunto) {
        this.assunto = assunto;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getDtCompromisso() {
        return dtCompromisso;
    }

    public void setDtCompromisso(Date dtCompromisso) {
        this.dtCompromisso = dtCompromisso;
    }

    public Date getDtCadastro() {
        return dtCadastro;
    }

    public void setDtCadastro(Date dtCadastro) {
        this.dtCadastro = dtCadastro;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }
    
   public boolean getConcluido()
   {
       if(situacao != null)
       {
           if(situacao.equals("F"))
           {
               return true;
           }else
           {
               return false;
           }
       }
       return false;
   }
    
}
