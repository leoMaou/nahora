/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.atividade.nahora.controlador;

import br.com.atividade.nahora.modelo.Compromisso;
import br.com.atividade.nahora.modelo.Usuario;
import br.com.atividade.nahora.util.RepositorioCompromisso;
import br.com.atividade.nahora.util.RepositorioUsuario;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author emerson
 */
@WebServlet(name = "ControladorServlet", urlPatterns = {"/fazerLogin", "/cadastrarNovoUsuario", "/listarCompromisso", "/removerCompromisso", "/novoCompromisso", "/salvarCompromisso", "/editarCompromisso", "/logout"})
public class ControladorServlet extends HttpServlet {

    private static Integer codUsuario;
    private static  List<Compromisso> listaCompromisso = new ArrayList<>();
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String jsp = null;
        Boolean erro = false;
        try{
            
            
             if (request.getRequestURI().endsWith("/fazerLogin")) {
                 jsp = fazerLogin(request, response);
             }
             if (request.getRequestURI().endsWith("/cadastrarNovoUsuario")) {
                 jsp = cadastrarUsuario(request, response);
             }
             if (request.getRequestURI().endsWith("/editarCompromisso")) {
                 jsp = editarCompromisso(request, response);
             }
             if (request.getRequestURI().endsWith("/listarCompromisso")) {
                 jsp = listarCompromisso(request, response);
             }
             if (request.getRequestURI().endsWith("/novoCompromisso")) {
                 jsp = "cadastroCompromisso.jsp";
             }
              if (request.getRequestURI().endsWith("/salvarCompromisso")) {
                 jsp = salvarCompromisso(request, response);
             }
             if (request.getRequestURI().endsWith("/removerCompromisso")) {
                 jsp = removerCompromisso(request, response);
             }
             if (request.getRequestURI().endsWith("/logout")) {
                 jsp = sair(request, response);
             }
             
           }catch(Exception e)
           {
               erro = true;
               request.setAttribute("mensagem", e.getMessage());
               request.getRequestDispatcher("erro.jsp").forward(request, response);
           }
        
        if(!erro)
        {
            if(jsp == null)
            {
                request.getRequestDispatcher("login.jsp").forward(request, response);
            }else
            {
                request.getRequestDispatcher(jsp).forward(request, response);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    
    private String fazerLogin(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        String email = request.getParameter("email");
        String senha = request.getParameter("senha");
        
        RepositorioUsuario repoUsuario = new RepositorioUsuario();
        
        int codigo = repoUsuario.verificaLogin(email, senha);
        
        if(codigo > 0)
        {
            Usuario usuarioLogado = repoUsuario.getUsuario(codigo);
            HttpSession sessao = request.getSession();
            sessao.setAttribute("codusuario", usuarioLogado.getCodigo());
            
          
          return "listarCompromisso";
        }else
        {
            request.setAttribute("mensagem", "Não foi possivel efetuar Login: Email ou senha invalido");
            return "login.jsp";
        }
    }
    
    private String cadastrarUsuario(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        String nome = request.getParameter("nome");
        String email = request.getParameter("email");
        String senha = request.getParameter("senha");
        
        Usuario usuario = new Usuario(nome, email, senha);
        RepositorioUsuario repoUsuario = new RepositorioUsuario();
        repoUsuario.salvar(usuario);
        request.setAttribute("usuarioLogin", usuario);
        
        return "login.jsp";
    }
    
     private String listarCompromisso(HttpServletRequest request, HttpServletResponse response) throws Exception
     {
        if(codUsuario == null)
        {
            HttpSession sessao = request.getSession();
            codUsuario = (Integer) sessao.getAttribute("codusuario");
        }
         
         RepositorioCompromisso repocomp = new RepositorioCompromisso();
         this.listaCompromisso = repocomp.getCompromissos(this.codUsuario); 
         request.setAttribute("listaCompromisso", listaCompromisso);
         
         return "agenda.jsp";
     }
     
     private String salvarCompromisso(HttpServletRequest request, HttpServletResponse response) throws Exception
     {
        if(codUsuario == null)
         {
            HttpSession sessao = request.getSession();
            codUsuario = (Integer) sessao.getAttribute("codusuario");
         }
        
         String codigo = request.getParameter("codigo");
         String assunto = request.getParameter("assunto");
         String descricao = request.getParameter("descricao");
         String situacao = request.getParameter("situacao");
         String dtCompromisso = request.getParameter("dtCompromisso");
         
         
         if(!dtCompromisso.equals(""))
         {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date data = formatter.parse(dtCompromisso);
            
            Compromisso compromisso = new Compromisso();
            
            if(!codigo.equals(""))
            {
              compromisso.setCodigo(Integer.parseInt(codigo));
            }
            
            if(situacao == null)
            {
             compromisso.setSituacao("A");
            }else
            {
             compromisso.setSituacao("F");
            }
            
            compromisso.setAssunto(assunto);
            compromisso.setDescricao(descricao);
            compromisso.setDtCompromisso(data);
           
            compromisso.setCodigoUsuario(codUsuario);
         
            RepositorioCompromisso repocomp = new RepositorioCompromisso();
            repocomp.salvar(compromisso);
         
         
            return "listarCompromisso";
         
         }else
         {
             request.setAttribute("mensagem", "Insira uma data valida!");
             return "cadastroCompromisso.jsp";
         }
     }
     
     private String removerCompromisso(HttpServletRequest request, HttpServletResponse response) throws Exception
     {
          Integer codigo = Integer.valueOf(request.getParameter("codigo"));
          RepositorioCompromisso repocomp = new RepositorioCompromisso();
          Compromisso comp = repocomp.getCompromisso(codigo);
          repocomp.remover(comp);
          listaCompromisso = null;
          
          return "listarCompromisso";
     }
     
    
     private String editarCompromisso(HttpServletRequest request, HttpServletResponse response) throws Exception
     {
          Integer codigo = Integer.valueOf(request.getParameter("codigo"));
          RepositorioCompromisso repocomp = new RepositorioCompromisso();
          Compromisso comp = repocomp.getCompromisso(codigo);
          request.setAttribute("compromissoedicao", comp);
          return "cadastroCompromisso.jsp";
     }
     
     private String sair(HttpServletRequest request, HttpServletResponse response) throws Exception
     {
         HttpSession sessao = request.getSession();
         sessao.invalidate();
         
         return "login.jsp";
     }

}
