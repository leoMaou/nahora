/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.atividade.nahora.util;
import br.com.atividade.nahora.modelo.Compromisso;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author emerson
 */
public class RepositorioCompromisso {
    
    public static void main(String[] args) throws Exception{
        
        
        List<Compromisso> lista = new ArrayList<>();
        lista = getCompromissos(2);
        
        
        for(int i = 0; i < lista.size(); i++)
        {
            System.out.println("assunto: " + lista.get(i).getAssunto()); 
            System.out.println("data: " + lista.get(i).getDtCompromisso());
            System.out.println("\n\n");
        }
        
    }
    
     public static void salvar(Compromisso comp) throws Exception
    {
        Compromisso eq = getCompromisso(comp.getCodigo());
        
        if(eq == null)
        {
            insert(comp);
        }else
        {
            update(comp);
        }
    }
   
    private static void insert(Compromisso comp) throws Exception
    {
        String sql = "insert into COMPROMISSO values(?,?,?,?,?,?,?)";
        CriarConexao conexao = new CriarConexao();
        Integer codigo = buscaMaiorCodigo();
        
        try(Connection c = conexao.getConnection();
                PreparedStatement ps = c.prepareStatement(sql);)
        {
            ps.setInt(1, codigo+1);
            ps.setInt(2, comp.getCodigoUsuario());
            ps.setString(3, comp.getAssunto());
            ps.setString(4, comp.getDescricao());
            ps.setDate(5, new Date(comp.getDtCompromisso().getTime()) );
            //data cadastro
            ps.setDate(6, new Date(new java.util.Date().getTime()) );
            ps.setString(7, comp.getSituacao());
            ps.execute();
        }
    }
    
     //codigo, codigousuario, assunto, descricao, dtcompromisso, dtcadastro, situacao
    private static void update(Compromisso comp) throws Exception
    {
        String sql = "update compromisso set assunto=?, descricao=?, dtcompromisso=?, situacao=? where codigo=?";
        CriarConexao conexao = new CriarConexao();
         
        try(Connection c = conexao.getConnection();
                PreparedStatement ps = c.prepareStatement(sql);)
        {
            ps.setString(1, comp.getAssunto());
            ps.setString(2, comp.getDescricao());
            ps.setDate(3, new Date(comp.getDtCompromisso().getTime()));
            ps.setString(4, comp.getSituacao());
            ps.setInt(5, comp.getCodigo());
            ps.execute();
        }
    }
    
    private static Integer buscaMaiorCodigo()
    {
        Integer codigo = null;
        String sql = "select max(codigo) codigo from compromisso";
         CriarConexao conexao = new CriarConexao();
         
        try(Connection c = conexao.getConnection();
                PreparedStatement ps = c.prepareStatement(sql);)
        {
            ResultSet rs = ps.executeQuery();
            if(rs.next())
            {
                codigo = rs.getInt("codigo");
            }
            
        }catch(Exception ex)
        {
            return null;
        }
        
        return codigo;
    }
    
    public static void remover(Compromisso comp) throws Exception
    {
        String sql = "delete from compromisso where codigo=?";
        CriarConexao conexao = new CriarConexao();
        try(Connection c = conexao.getConnection();
                PreparedStatement ps = c.prepareStatement(sql);)
        {
            ps.setInt(1, comp.getCodigo());
            ps.execute();
        }
        
    }
    
    public static Compromisso getCompromisso(Integer codigo) throws Exception
    {
        Compromisso compromisso = null;
        String sql = "select codigo, codigousuario, assunto, descricao, dtcompromisso, dtcadastro, situacao from compromisso where codigo=?";
        
        CriarConexao conexao = new CriarConexao();
        try(Connection c = conexao.getConnection();
                PreparedStatement ps = c.prepareStatement(sql);)
        {
            ps.setInt(1, codigo);
            ResultSet rs = ps.executeQuery();
            if(rs.next())
            {
                compromisso = new Compromisso();
                compromisso.setCodigo(codigo);
                compromisso.setCodigoUsuario(rs.getInt("codigousuario"));
                compromisso.setAssunto(rs.getString("assunto"));
                compromisso.setDescricao(rs.getString("descricao"));
                compromisso.setDtCompromisso(rs.getDate("dtcompromisso"));
                compromisso.setDtCadastro(rs.getDate("dtcadastro"));
                compromisso.setSituacao(rs.getString("situacao"));
            }
        }
        
        return compromisso;
    }
    
    
    public static List<Compromisso> getCompromissos(Integer codigoUsuario) throws Exception
    {
        List<Compromisso> lista = new ArrayList<>();
        String sql = "select codigo, codigousuario, assunto, descricao, dtcompromisso, dtcadastro, situacao from compromisso where codigousuario=? and situacao <> 'F' order by dtCompromisso";
        CriarConexao conexao = new CriarConexao();
        
        try(Connection c = conexao.getConnection();
                PreparedStatement ps = c.prepareStatement(sql);)
        {
            ps.setInt(1, codigoUsuario);
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                Compromisso compromisso = new Compromisso();
                compromisso.setCodigo(rs.getInt("codigo"));
                compromisso.setCodigoUsuario(rs.getInt("codigousuario"));
                compromisso.setAssunto(rs.getString("assunto"));
                compromisso.setDescricao(rs.getString("descricao"));
                compromisso.setDtCompromisso(rs.getDate("dtcompromisso"));
                compromisso.setDtCadastro(rs.getDate("dtcadastro"));
                compromisso.setSituacao(rs.getString("situacao"));
                lista.add(compromisso);
            }
        }
        
        return lista;
    }
    
}
