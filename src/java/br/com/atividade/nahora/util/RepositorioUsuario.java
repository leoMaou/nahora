/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.atividade.nahora.util;
import java.sql.Connection;

import br.com.atividade.nahora.modelo.Usuario;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author emerson
 */
public class RepositorioUsuario {
    
   
    public static void main(String[] args) throws Exception{
        
        Usuario u = new Usuario("teste4", "emdsaail 321das treta", "Sedasdanha a321qui");
        salvar(u);
        
        u = new Usuario("NOEM TESTE", "email", "senha");
        salvar(u);
        
        
        u = getUsuario(2);
        u.setEmail("teste update baludo");
        salvar(u);
        
        int codigo = verificaLogin("email", "senha");
        if(codigo > 0)
        {
            System.out.println("usuario com codigo " + codigo + " ecnotrado");
            u = getUsuario(codigo);
            System.out.println(u.getNome());
        }else
        {
            System.out.println("nada encontrado");
        }
        
      List<Usuario> usuarios = new ArrayList<>();
      usuarios = getUsuarios();
      
      System.out.println("usuarios");
      for(int i = 0; i < usuarios.size(); i++)
      {
          System.out.println( usuarios.get(i).getCodigo() +  " -  " + usuarios.get(i).getNome());
      }
        
    }
    
    public static void salvar(Usuario u) throws Exception
    {
         Usuario eq = getUsuario(u.getCodigo());
        if(eq == null)
        {
          System.out.println("Inserindo");
            insert(u);
        } else
        {
            update(u);
        }
    }
    
    private static void insert(Usuario u) throws Exception
    {
        String sql = "insert into USUARIO values(?, ?, ?, ?)";
        CriarConexao conexao = new CriarConexao();
        Integer codigo = buscaMaiorCodigo();
        
        try(Connection c = conexao.getConnection();
                PreparedStatement ps = c.prepareStatement(sql);)
        {
            
            ps.setInt(1, codigo+1);
            ps.setString(2, u.getNome());
            ps.setString(3, u.getEmail());
            ps.setString(4, u.getSenha());
            ps.execute();
        }
    
    }
    
    private static void update(Usuario u) throws Exception
    {
        String sql = "update usuario set nome=?, email=?, senha=? where codigo=?";
         CriarConexao conexao = new CriarConexao();
         
        try(Connection c = conexao.getConnection();
                PreparedStatement ps = c.prepareStatement(sql);)
        {
            ps.setString(1, u.getNome());;
            ps.setString(2, u.getEmail());
            ps.setString(3, u.getSenha());
            ps.setInt(4, u.getCodigo());
            ps.execute();
        }
        
    }
    
    
    private static Integer buscaMaiorCodigo()
    {
        Integer codigo = null;
        String sql = "select max(codigo) codigo from USUARIO";
        
        CriarConexao conexao = new CriarConexao();
        try(Connection c = conexao.getConnection();
                PreparedStatement ps = c.prepareStatement(sql);)
        {
            ResultSet rs = ps.executeQuery();
            if(rs.next())
            {
                codigo = rs.getInt("codigo");
            }
        }catch(Exception ex)
        {
            return null;
        }
        
        return codigo;
    
    }
    
    
    //se voltar algo maior que 0, quer dizer que o usuario existe, o retorno é o codigo do usuario
    public static int verificaLogin(String email, String senha) throws Exception
    {
        int codigo = 0;
        String sql = "select codigo from usuario where email=? and senha=?";
        
        CriarConexao conexao = new CriarConexao();
        try(Connection c = conexao.getConnection();
                PreparedStatement ps = c.prepareStatement(sql);)
        {
            ps.setString(1, email);
            ps.setString(2, senha);
            ResultSet rs = ps.executeQuery();
            if(rs.next())
            {
                codigo = rs.getInt("codigo");
            }
        }
        
        return codigo;
    }
    
    public static void remover(Usuario u) throws Exception
    {
        String sql = "delete from usuario where codigo=?";
        CriarConexao conexao = new CriarConexao();
        try(Connection c = conexao.getConnection();
                PreparedStatement ps = c.prepareStatement(sql);)
        {
            ps.setInt(1, u.getCodigo());
            ps.execute();
        }
        
    }
    
    public static Usuario getUsuario(Integer codigo) throws Exception
    {
        Usuario usuario = null;
        String sql = "select codigo, nome, email, senha from usuario where codigo=?";
        
        CriarConexao conexao = new CriarConexao();
        try(Connection c = conexao.getConnection();
                PreparedStatement ps = c.prepareStatement(sql);)
        {
            ps.setInt(1, codigo);
            ResultSet rs = ps.executeQuery();
            if(rs.next())
            {
                usuario = new Usuario();
                usuario.setCodigo(codigo);
                usuario.setEmail(rs.getString("email"));
                usuario.setNome(rs.getString("nome"));
                usuario.setSenha("senha");
            }
        }
        
        return usuario;
    }
    
    public static List<Usuario> getUsuarios() throws Exception
    {
        List<Usuario> lista = new ArrayList<>();
        String sql = "select codigo, nome, email, senha from usuario";
        CriarConexao conexao = new CriarConexao();
        try(Connection c = conexao.getConnection();
                PreparedStatement ps = c.prepareStatement(sql);)
        {
            ResultSet rs = ps.executeQuery();
             while(rs.next()) 
             {
                Usuario usuario = new Usuario();
                usuario.setCodigo(rs.getInt("codigo"));
                usuario.setNome(rs.getString("nome"));
                usuario.setEmail(rs.getString("email"));
                usuario.setSenha(rs.getString("senha"));
                lista.add(usuario);
             }
        }
        return lista;
    }
    
    
    
}
